#![allow(clippy::result_large_err)]

use aws_config;
use aws_sdk_sts::{ Client, Error};
// use aws_sdk_s3 as s3;

async fn get_caller_identity(client: &Client) -> Result<(), Error> {
    let response = client.get_caller_identity().send().await?;
    println!(
        "Success! AccountId = {}",
        response.account().unwrap_or_default()
    );
    println!(
        "Success! AccountArn = {}",
        response.arn().unwrap_or_default()
    );
    println!(
        "Success! UserID = {}",
        response.user_id().unwrap_or_default()
    );

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let shared_config = aws_config::load_from_env().await;
    let client = Client::new(&shared_config);
    get_caller_identity(&client).await
}
